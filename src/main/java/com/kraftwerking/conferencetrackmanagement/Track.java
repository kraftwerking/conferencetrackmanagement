package com.kraftwerking.conferencetrackmanagement;

public class Track {
	
	//Full day
    private Session morningSession;
    private Session afternoonSession;
    private Session lunchSession;
    
	public Track(Session morningSession, Session lunchSession, Session afternoonSession) {
		super();
		this.morningSession = morningSession;
		this.lunchSession = lunchSession;
		this.afternoonSession = afternoonSession;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(morningSession);
		sb.append(lunchSession);
		sb.append(afternoonSession);
		return sb.toString();
	}
	
}
