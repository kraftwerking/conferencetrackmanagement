package com.kraftwerking.conferencetrackmanagement;

import java.util.ArrayList;
import java.util.List;

public class Session {

    private List<Talk> talks;
    
    public Session() {
    	talks = new ArrayList<Talk>();
    }

    public void addTalk(Talk talk) {
    	talks.add(talk);
    }

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Talk talk : talks) {
			//System.out.println(talk);
			sb.append(talk + "\n");
		}
		return sb.toString();
	}
    
    
}