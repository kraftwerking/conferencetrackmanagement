package com.kraftwerking.conferencetrackmanagement;

import java.io.BufferedReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ConferenceScheduler {

	private static final String MORNING_START_TIME = "09:00 AM";
	private static final String LUNCH_START_TIME = "12:00 PM";
	private static final String AFTERNOON_START_TIME = "01:00 PM";
	private static final String NETWORKING_START_TIME = "05:00 PM";
	private static final int MORNING_MAX_LEN = 180;
	private static final int AFTERNOON_MAX_LEN = 240;

	private static int lenCount = 0;
	private List<Talk> talks = new ArrayList<Talk>();
	
	public Conference schedule(BufferedReader reader) throws NumberFormatException, IOException, ParseException {
		// TODO Auto-generated method stub
		String line;
		String title;
		String time;
		int len;
		Talk talk = null;


		while ((line = reader.readLine()) != null) {
			// TODO: error checking when parsing input file
			title = line.substring(0, line.lastIndexOf(" ")).trim();
			time = line.substring(line.lastIndexOf(" ") + 1).trim();
			if (time.equals("lightning")) {
				len = 5;
			} else {
				len = Integer.parseInt(time.replace("min", ""));
			}
			talk = new Talk(title, len);
			talks.add(talk);

		}
		
		Track tr1 = createTrack();
		Track tr2 = createTrack();
		
		Conference c = new Conference();
		c.addTrack(tr1);
		c.addTrack(tr2);

		return c;
	}

	private Track createTrack()  throws IOException, ParseException  {
		lenCount = 0;
		Session morningSession = new Session();
		Session lunchSession = new Session();
		Session afternoonSession = new Session();
		
		// TODO Auto-generated method stub
		String currStartTime = (String) MORNING_START_TIME;

		// create morning session
		Iterator<Talk> iter = talks.iterator();

		while (iter.hasNext()) {
			Talk tlk = iter.next();
			tlk.setStartTime(currStartTime);
			lenCount += tlk.getLen();
			if (lenCount > MORNING_MAX_LEN) {
				break;
			} else {
				// get next start time - util
				currStartTime = tlk.getNextStartTime(currStartTime, tlk.getLen());
				morningSession.addTalk(tlk);
				iter.remove();
			}
		}
		
		currStartTime = LUNCH_START_TIME;
		//create lunch
		Talk lunch = new Talk("Lunch",60,LUNCH_START_TIME);
		lunchSession.addTalk(lunch);
		
		// create afternoon session
		iter = talks.iterator();
		currStartTime = AFTERNOON_START_TIME;
		lenCount=0;
		
		while (iter.hasNext()) {
			Talk tlk = iter.next();
			tlk.setStartTime(currStartTime);
			lenCount += tlk.getLen();
			if (lenCount > AFTERNOON_MAX_LEN) {
				break;
			} else {
				// get next start time - util
				currStartTime = tlk.getNextStartTime(currStartTime, tlk.getLen());
				afternoonSession.addTalk(tlk);
				iter.remove();
			}
		}
		Talk networking = new Talk("Networking Event", 60, NETWORKING_START_TIME);
		afternoonSession.addTalk(networking);
		
		Track tr = new Track(morningSession, lunchSession, afternoonSession);
		return tr;
		
	}
}
