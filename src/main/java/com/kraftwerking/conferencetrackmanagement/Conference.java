package com.kraftwerking.conferencetrackmanagement;

import java.io.BufferedReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class Conference {
    private List<Track> tracks;

    public Conference() {
        tracks = new ArrayList<Track>();
    }

    public void addTrack(Track track) {
        tracks.add(track);
    }

	public Conference scheduleConference(BufferedReader reader) throws IOException, ParseException {
		ConferenceScheduler conferenceScheduler = new ConferenceScheduler();
		Conference c = conferenceScheduler.schedule(reader);
		return c;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		int count=1;
		for (Track track : tracks) {
			sb.append("Track " + count + ":\n");
			sb.append(track + "\n");
			count++;
		}
		return sb.toString();
	}
    


}
