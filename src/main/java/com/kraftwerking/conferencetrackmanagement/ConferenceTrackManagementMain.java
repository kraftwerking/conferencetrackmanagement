package com.kraftwerking.conferencetrackmanagement;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;


public class ConferenceTrackManagementMain {

    public static void main(String[] args) throws ParseException {
        if (args.length < 1) {
            System.out.println("Please provide path to input file.");
            System.exit(1);
        }

        File inputFile = new File(args[0]);
        try {
            BufferedReader reader = new BufferedReader(new FileReader(inputFile));
            Conference conference = new Conference().scheduleConference(reader);
            System.out.println(conference);
            reader.close();
        } catch (IOException e) {
        	System.out.println("Cannot read from input file: " + inputFile.getAbsolutePath());
            System.exit(1);
        }
    }

}
