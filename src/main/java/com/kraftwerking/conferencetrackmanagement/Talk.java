package com.kraftwerking.conferencetrackmanagement;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Talk {

	private String title;
	private String start;

	SimpleDateFormat df = new SimpleDateFormat("hh:mm a");
	Calendar cal = Calendar.getInstance();

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	private int len;

	public Talk(String title, int len) {
		// TODO Auto-generated constructor stub
		this.title = title;
		this.len = len;
	}

	public Talk(String title, int len, String lunchStartTime) {
		// TODO Auto-generated constructor stub
		this.title = title;
		this.len = len;
		this.start = lunchStartTime;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getLen() {
		return len;
	}

	public void setLen(int len) {
		this.len = len;
	}

	public void setStartTime(String currStartTime) throws ParseException {
		// TODO Auto-generated method stub
		//TODO joda time
		Date d = df.parse(currStartTime);
		cal.setTime(d);
		String startTime = df.format(cal.getTime());
		this.setStart(startTime);
		
	}
	
	public String getNextStartTime(String currStartTime, int len2) throws ParseException {
		// TODO Auto-generated method stub
		//TODO joda time
		Date d = df.parse(currStartTime);
		cal.setTime(d);
		cal.add(Calendar.MINUTE, len2);
		String startTime = df.format(cal.getTime());
		return startTime;
		
	}

	@Override
	public String toString() {
		String length = "";
		if(len==5){
			length = "lightning";
		} else if (title.equals("Lunch") || title.equals("Networking Event")) {
			length = "";
		} else {
			length = len+"min";
		}
		
		
		return start + " " + title + " " + length;
	}
	
	

}
