package com.kraftwerking.conferencetrackmanagement.test;

import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;

import org.junit.Before;
import org.junit.Test;

import com.kraftwerking.conferencetrackmanagement.Conference;

public class ConferenceTrackManagementTest {

	@Before
	public void init() throws ParseException {
		File inputFile = new File("src/main/resources/input.txt");
		try {
			BufferedReader reader = new BufferedReader(new FileReader(inputFile));
			Conference conference = new Conference().scheduleConference(reader);
			try (PrintWriter out = new PrintWriter("src/main/resources/output.txt")) {
				out.println(conference.toString());
			}
			reader.close();
		} catch (IOException e) {
			System.out.println("Cannot read from input file: " + inputFile.getAbsolutePath());
			System.exit(1);
		}
	}

	@Test
	public void testConferenceTrackManagementMain() throws ParseException, IOException {
		String res = new String(Files.readAllBytes(Paths.get("src/main/resources/output.txt")));
		System.out.println(res);
		String expected = new String(Files.readAllBytes(Paths.get("src/main/resources/expected.txt")));
		System.out.println(expected);

		assertTrue(expected.trim().equals(res.trim()));
	}

}